import axios from 'axios'
import cli from "clui";
import cheerio from "cheerio";
import fs from 'fs'

const spawn = require('child_process').spawn
import {config} from './app'

const imageRegexp = new RegExp('\\.(jpg|gif|png|jpeg|tiff|psd|bmp|heif)$', 'i')
export default {
    download: function (url, post_data) {
        if (imageRegexp.test(url)) {
            this.image(url, post_data)
        }
        else if (/https?:\/\/(www.)?imgur.com(\/a)?\//i.test(url)) {
            this.imgur(url, post_data)
        }
        else if (/https?:\/\/(www.)?erome.com\/a\//i.test(url)) {
            this.erome(url, post_data)
        }
        else {
            this.youtubedl(url, post_data)
        }
    },
    erome: function (url, post_data) {

        axios({
            url: url,
            method: 'GET',
            responseType: 'text'
        })
            .then(r => {

                const $ = cheerio.load(r.data);
                const hrefs = []
                $('div.img .img-front').each(function (i, e) {
                    hrefs.push('https:' + $(this).attr('src'))
                })
                $('div.video source').each(function (i, e) {
                    hrefs.push('https:' + $(this).attr('src'))
                })
                hrefs.filter(e => e).forEach((i, it) => {
                    this.image(i, {...post_data, title: it + ' ' + post_data.title})
                })
            })
            .catch(e => {
                console.error(e)
            })
    },
    imgur: function (url, post_data) {
        axios({
            url: url,
            method: 'GET',
            responseType: 'text'
        })
            .then(r => {
                const $ = cheerio.load(r.data);
                const hrefs = []
                $('a.zoom').each(function (i, e) {
                    hrefs.push('https:' + $(this).attr('href'))
                })
                if (hrefs.length === 0) {
                    if ($('.post-image img').attr('src').length > 0)
                        hrefs.push('https:' + $('.post-image img').attr('src'))
                }
                $('.video-elements source').each(function (i, e) {
                    hrefs.push('https:' + $(this).attr('src'))
                })

                hrefs.filter(e => e).forEach((i, it) => {
                    this.image(i, {...post_data, title: it + ' ' + post_data.title})
                })
            })
            .catch(e => {
                console.error(e)
            })
    },
    youtubedl: function (url, post_data) {

        const reg = new RegExp('\\[download\\]\\s*([\\d\\.]*)% of ([\\d\\.]*[a-zA-Z]*) at\\s* ([\\d\\.]*[a-zA-Z\\/]*) ETA (\\d\\d:\\d\\d)')
        config.processCount++;
        config.youtubedlRunning++;
        const spawned = spawn('./lib/youtube-dl', [url, '--restrict-filenames', '-o' + config.path + '%(title)s.%(ext)s'])


        var prog = new cli.Progress(20);
        spawned.stdout.on('data', (data) => {
            const s = data.toString()
            //console.log(s)
            if (reg.test(s)) {
                const match = s.match(reg)
                config.progressData[url] = {
                    title: post_data.title || '',
                    progressBar: prog,
                    percent: match[1],
                    total: match[2],
                    speed: match[3],
                    eta: match[4]
                }
                //console.log(progressData)
            }
        })
        spawned.stderr.on('data', (data) => {

            config.progressData[url] = {
                title: post_data.title || '',
                type: 'ERROR',
                eta: '00:00',
                message: data.toString(),
                url: post_data.url
            }
        })
        spawned.on('exit', (data) => {
            config.processCount--;
            config.youtubedlRunning--;
            if (!config.progressData[url])
                config.progressData[url] = {
                    title: post_data.title || '',
                    type: 'EXIT',
                    eta: '00:00'
                }
        })
    },
    image: async function (url, post_data) {
        config.processCount++;
        const filename = url.split('/').reverse()[0]
        const filePath = config.path + filename

        try {
            fs.statSync(filePath)
            config.alreadyDownloaded++;
            return config.processCount--;
        } catch (e) {

        }

        let response
        try {
            response = await axios({
                method: 'GET',
                url: url,
                responseType: 'stream'
            })
        } catch (e) {
            config.processCount--;
            return;
        }


        response?.data.pipe(fs.createWriteStream(filePath))
        response?.data.on('end', () => {
            config.imagesDownloaded++;
            config.processCount--;
        })


    }


}