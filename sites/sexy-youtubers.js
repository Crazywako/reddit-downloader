import cli from "clui";
import axios from 'axios'
import clc from "cli-color";
import inquirer from "inquirer";
import shell from "shelljs";
import cheerio from "cheerio";
import { config } from '../app'
import output from '../console'
import dl from '../downloader'

export default {

    init: function(){
        config.nextPageFunction = this.nextPage
        cli.Clear();
        output.createLogo()
        new cli.Line().column(config.logo, 'console', [clc.yellow.bold]).output();
        inquirer.prompt([
            {
                type: 'input',
                message: 'How many pages we will download automatically?',
                name: 'pages',
                default: '1'
            }
        ]).then(answers => {

            config.after = void 0;
            config.automaticPages = parseInt(answers['pages'])
            const videoBaseUrl = 'videos'
            config.path = videoBaseUrl + '/' + 'sexy-youtubers' + '/'

            try {
                fs.statSync(config.path)

            } catch (e) {
                shell.mkdir('-p', config.path)
            }

            output.stopConsoleUpdates()
            this.nextPage();
            output.startConsoleUpdates()
        })
    },
    nextPage: () => {
        config.alreadyDownloaded = 0;
        config.youtubedlRunning = 0;
        config.imagesDownloaded = 0;
        config.sexyYoutubersPage++;
        axios({
            method: 'GET',
            url: 'https://www.sexy-youtubers.com/homepage-1/page/' + config.sexyYoutubersPage + '/',
            responseType: 'text'
        }).then(r => {
            if (r.status !== 200) {
                console.error('Couldn\'t retrieve Sexy Youtubers homepage page. Please make sure given URL is right.')
            } else {
                config.pagesRan++;
                config.processCount = 0;


                const $ = cheerio.load(r.data);


                $('h3.td-module-title > a').each(async function (i, e) {
                    const videoFiles = []
                    const response = await axios({
                        method: 'GET',
                        url: $(this).attr('href'),
                        responseType: 'text'
                    })

                    const $$ = cheerio.load(response.data);
                    $$('.size-full').each(function (i, e) {
                        const _url = $(this).attr('src')
                        const post_data = {title: _url.split('/').pop()}
                        dl.download(_url, post_data)
                    })

                    $$('iframe').each(function (i, e) {
                        const _url = $(this).attr('src')

                        if (/openload\.co/.test(_url) === false) return false;
                        if (videoFiles.includes(_url)) return false;
                        videoFiles.push(_url)
                    })
                    videoFiles.forEach(_url => {
                        const post_data = {title: _url.split('/').pop()}
                        dl.download(_url, post_data)
                    })
                })
            }
        })
    }

}