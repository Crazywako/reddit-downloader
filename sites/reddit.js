import axios from 'axios'
import cli from "clui";
import clc from "cli-color";
import inquirer from "inquirer";
import shell from "shelljs";
import { config } from '../app'
import output from '../console'
import dl from '../downloader'



export default {
    init : function() {
        config.nextPageFunction = this.nextPage
        cli.Clear();
        output.createLogo()

        new cli.Line().column(config.logo, 'console', [clc.yellow.bold]).output();
        inquirer.prompt([{
            type: 'input',
            message: 'Which subreddit you would like to download?',
            name: 'subreddit'
        }, {
            type: 'list',
            message: 'How to sort?',
            name: 'sort',
            choices: [
                {name: 'Hot', value: 'hot'},
                {name: 'Rising', value: 'rising'},
                {name: 'New', value: 'new'},
                {name: 'Top', value: 'top'},
                {name: 'Top - All Time', value: 'top_alltime'},

            ]
        },
            {
                type: 'input',
                message: 'How many pages we will download automatically?',
                name: 'pages',
                default: '1'
            }
        ]).then(answers => {

            config.after = void 0;
            config.subreddit = answers['subreddit']
            config.automaticPages = parseInt(answers['pages'])
            const videoBaseUrl = 'videos'
            config.path = videoBaseUrl + '/' + config.subreddit + '/'

            try {
                fs.statSync(config.path)

            } catch (e) {
                shell.mkdir('-p', config.path)
            }
            output.createLogo();
            const sort = answers['sort'] === 'top_alltime' ? 'top' : answers['sort']
            const time = answers['sort'] === 'top_alltime' ? 't=all' : ''
            config.url = 'https://reddit.com/r/' + answers['subreddit'] + '/' + sort + '.json?' + time
            this.nextPage();
            output.startConsoleUpdates()
        })
    },
    nextPage: () => {
        config.alreadyDownloaded = 0;
        config.youtubedlRunning = 0;
        config.imagesDownloaded = 0
        axios({
            method: 'GET',
            url: config.url.toString(),
            params: {
                count: 25,
                after: config.after
            }

        }).then(r => {
            if (r.status !== 200) {
                console.error('Couldn\'t retrieve Reddit JSON page. Please make sure given URL is right.')
            } else {
                config.pagesRan++;
                config.processCount = 0;

                config.after = r.data.data.after;

                r.data.data?.children.forEach(r => {
                    dl.download(r.data.url, r.data)
                })
            }
        })
    }
}