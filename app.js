import cli from 'clui'
import clc from 'cli-color'
import inquirer from 'inquirer'
import output from './console'

import reddit from './sites/reddit'
import sexyYoutubers from './sites/sexy-youtubers'


export let config = {
    url: '',
    subreddit: '',
    logo: '',
    progressData: {},
    path: '',
    running: '',
    processCount: -1,
    consoleUpdateInterval: 0,
    imagesDownloaded: 0,
    automaticPages: 1,
    pagesRan: 0,
    after: '',
    sexyYoutubersPage: 0,
    type: 'subreddit',
    alreadyDownloaded: 0,
    youtubedlRunning: 0,
    nextPageFunction: () => {

    }
}
let _oConfig = {...config}

askSite()

export function askSite() {
    // Reset config
    config = _oConfig
    output.createLogo()
    //cli.Clear();
    new cli.Line().column(config.logo, 'console', [clc.yellow.bold]).output();

    inquirer.prompt([{
        type: 'list',
        message: 'Which site?',
        name: 'site',
        choices: [
            {name: 'Reddit', value: 'reddit'},
            {name: 'Sexy Youtubers', value: 'sexyyoutubers'},
        ]
    }]).then(answers => {
        if (answers['site'] === 'reddit') {
            config.type = 'subreddit'
            reddit.init()
        } else {
            config.type = 'sexy-youtubers'
            sexyYoutubers.init()
        }
    })
}
