import cli from "clui";
import { config, askSite } from './app.js'
import clc from "cli-color";
import inquirer from "inquirer";
const outputBuffer = new cli.LineBuffer({
    x: 0,
    y: 0,
    width: 'console',
    height: 'console'
});

export default {

    startConsoleUpdates: function() {
        this.consoleUpdateInterval = setInterval(() => {
            this.updateConsole()
        }, 2500)
    },
    stopConsoleUpdates: function() {
        clearInterval(this.consoleUpdateInterval)
    },
    updateConsole: async function () {
        cli.Clear();
        //await asciiOutput()
        this.createLogo()
        new cli.Line().column(config.logo, 'console', [clc.yellow.bold]).output();
        new cli.Line(outputBuffer).padding(5).column('Running... ' + config.processCount + ' downloads left. ' + config.youtubedlRunning + ' spawned processes. ' + config.alreadyDownloaded + ' files already downloaded.').output()
        new cli.Line().fill().output();

        if (config.processCount === 0) {
            setTimeout(() => {
                if (config.processCount === 0) {

                    config.progressData = {}
                    config.processCount = -1
                    this.confirmNextPage();
                }
                return;
            }, 1500)
        }

        if (config.imagesDownloaded !== 0) {
            new cli.Line().padding(5).column('Images downloaded', 50).padding(2).column('' + config.imagesDownloaded, 20).fill().output()
        }

        Object.keys(config.progressData).forEach(k => {
            const v = config.progressData[k]
            if (v.type === 'EXIT') {
                new cli.Line().padding(5).column(v.title, 50).padding(2).column('END').fill().output()
            }
            else if (v.type === 'ERROR') {
                new cli.Line().padding(5).column(v.title, 50).padding(2).column('ERROR').padding(2).column(JSON.stringify(v.url)).fill().output()
            }
            else {
                new cli.Line().padding(5).column(v.title, 50).padding(2).column(v.progressBar?.update(v.percent, 100) || '', 30).padding(2).column(v.total, 10).padding(2).column(v.speed, 15).padding(2).column(v.eta, 10).padding(2).fill().output()
            }
        })
        outputBuffer.output();
    },
    confirmNextPage: function() {
        if (config.pagesRan < config.automaticPages) {
            this.stopConsoleUpdates()
            config.nextPageFunction()
            this.startConsoleUpdates()
        } else {
            this.stopConsoleUpdates()
            inquirer.prompt([{
                type: 'input',
                message: 'How many pages more? Insert \'0\' to change subreddit.',
                name: 'nextpage'
            }]).then(answers => {
                let p = parseInt(answers['nextpage'])
                if (isNaN(p)) {
                    p = 1
                }
                if (p == 0) {

                    return askSite();

                }
                config.automaticPages += p

                config.nextPageFunction()
                this.startConsoleUpdates()
            })
            new cli.Line().output();
            new cli.Line().padding(5).column('You can also quit by pressing CTRL + C.').output()

        }
    },
    createLogo: function() {
        config.logo = `
===============================================================================================

#  ██████╗ ███████╗██████╗ ██████╗ ██╗████████╗                                               #
#  ██╔══██╗██╔════╝██╔══██╗██╔══██╗██║╚══██╔══╝                                               #
#  ██████╔╝█████╗  ██║  ██║██║  ██║██║   ██║                                                  #
#  ██╔══██╗██╔══╝  ██║  ██║██║  ██║██║   ██║                                                  #
#  ██║  ██║███████╗██████╔╝██████╔╝██║   ██║                                                  #
#  ╚═╝  ╚═╝╚══════╝╚═════╝ ╚═════╝ ╚═╝   ╚═╝                                                  #
#                                                                                             #
#  ██████╗  ██████╗ ██╗    ██╗███╗   ██╗██╗      ██████╗  █████╗ ██████╗ ███████╗██████╗      #
#  ██╔══██╗██╔═══██╗██║    ██║████╗  ██║██║     ██╔═══██╗██╔══██╗██╔══██╗██╔════╝██╔══██╗     #
#  ██║  ██║██║   ██║██║ █╗ ██║██╔██╗ ██║██║     ██║   ██║███████║██║  ██║█████╗  ██████╔╝     #
#  ██║  ██║██║   ██║██║███╗██║██║╚██╗██║██║     ██║   ██║██╔══██║██║  ██║██╔══╝  ██╔══██╗     #
#  ██████╔╝╚██████╔╝╚███╔███╔╝██║ ╚████║███████╗╚██████╔╝██║  ██║██████╔╝███████╗██║  ██║     #
#  ╚═════╝  ╚═════╝  ╚══╝╚══╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚══════╝╚═╝  ╚═╝     #
#                                                                                             #
#                                                                                             #
===============================================================================================
#                                                                                             #
# SUBREDDIT: ${(config.subreddit || '').padEnd(20)}                                                             #
#                                                                                             #
===============================================================================================

`
    },


}